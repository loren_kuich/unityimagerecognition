﻿using UnityEngine;
using System.Collections;
using CameraShot;
using UnityEngine.UI;

public class PostImage : MonoBehaviour
{
    float timer = 0;
    bool timerRunning = false;
    int[] imageSize = { 0, 0, 0, 0 };

    public IEnumerator getW(byte[] image, int index)
    {
        WWWForm form = new WWWForm();
        form.AddBinaryData("image", image, "image", "application/x-www-form-urlencoded");

        // API written in Wolfram Language
        WWW w = new WWW("https://www.wolframcloud.com/objects/6d8f15e1-3c59-4ac3-a0a4-508ba59a6c95", form);
        yield return w;
        if (w.error != null)
        {
            string error = w.error;
            print(error, index);
        }
        else
        {
            string txt = w.text;
            print(txt, index);
        }
    }

    public void CaptureTexture()
    {
        AndroidCameraShot.GetTexture2DFromCamera();
    }

    void OnEnable()
    {
        CameraShotEventListener.onImageLoad += OnImageLoad;
        CameraShotEventListener.onImageSaved += OnImageSave;
    }

    void OnDisable()
    {
        CameraShotEventListener.onImageLoad -= OnImageLoad;
        CameraShotEventListener.onImageSaved -= OnImageSave;
    }

    void OnImageSave(string path)
    {
        print("Saving image");
    }

    void OnImageLoad(string path, Texture2D tex)
    {
        toggleTimer();

        int compression = -25;
        for (int i = 1; i < 5; i++)
        {
            compression += 25;
            
            var bytes = tex.EncodeToJPG(compression);
            imageSize[i-1] = bytes.Length;
            StartCoroutine(getW(bytes, i));
        }
    }
    
    void print(string text, int index)
    {
        Debug.Log(text);
        gameObject.GetComponentsInChildren<Text>()[index].text += "\n" + text + "\nTime: " + timer + "\nSize: " + imageSize[index -1];
    }

    void toggleTimer()
    {
        timerRunning = !timerRunning;
    }

    void Update()
    {
        if (timerRunning)
            timer += Time.deltaTime;
    }
}
